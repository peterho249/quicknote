﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNote.Models
{
    /// <summary>
    /// This class to maintain tag information
    /// </summary>
    public class Tag
    {
        // Class member
        private string _name;
        private List<int> _noteIndexList;

        // Property
        public string Name { get => _name; set => _name = value; }
        public List<int> NoteIndexList { get => _noteIndexList; set => _noteIndexList = value; }

        // Method
    }
}
