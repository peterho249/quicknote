﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNote.Models
{
    /// <summary>
    /// This class to store tag and note list.
    /// </summary>
    public class NoteBook
    {
        // Class member
        private int _noteCount;
        private int _tagCount;
        private List<Tag> _tagList;
        private List<Note> _noteList;
        private List<int> _blankNoteIndex;

        // Property
        public int NoteCount { get => _noteCount; set => _noteCount = value; }
        public int TagCount { get => _tagCount; set => _tagCount = value; }
        public List<Tag> TagList { get => _tagList; set => _tagList = value; }
        public List<Note> NoteList { get => _noteList; set => _noteList = value; }
        public List<int> BlankNoteIndex { get => _blankNoteIndex; set => _blankNoteIndex = value; }

        // Method
    }
}
