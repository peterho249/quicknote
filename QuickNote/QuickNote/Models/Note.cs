﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNote.Models
{
    /// <summary>
    /// This class to maintain note information.
    /// </summary>
    public class Note
    {
        // Class member
        private string _content;
        private string _tagString;
        private DateTime _noteDate;

        // Property
        public string Content { get => _content; set => _content = value; }
        public string TagString { get => _tagString; set => _tagString = value; }
        public DateTime NoteDate { get => _noteDate; set => _noteDate = value; }

        // Method
    }
}
